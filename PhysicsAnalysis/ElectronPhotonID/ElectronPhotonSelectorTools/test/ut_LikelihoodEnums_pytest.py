#! /usr/bin/env python

# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

from ElectronPhotonSelectorTools.LikelihoodEnums import LikeEnum

print("Loose ", LikeEnum.Loose)
print("Medium ", LikeEnum.Medium)
print("Tight ", LikeEnum.Tight)

